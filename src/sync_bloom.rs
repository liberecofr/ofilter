// Copyright (C) 2024 Christian Mauduit <ufoot@ufoot.org>

use crate::bloom::Bloom;
use crate::params::Params;
use bitvec::prelude::BitVec;
use std::fmt;
use std::hash::Hash;
use std::sync::{Arc, RwLock};

/// Thread-safe Bloom filter.
///
/// Same as the standard Bloom filter,
/// but safe to use in concurrent environments.
///
/// # Examples
///
/// ```
/// use std::thread;
/// use ofilter::SyncBloom;
///
/// let filter: SyncBloom<usize> = SyncBloom::new(1_000);
///
/// let f = filter.clone();
/// let handle1 = thread::spawn(move || {
///     for i in 0..1_000 {
///         f.set(&i);
///     }
/// });
///
/// let f = filter.clone();
/// let handle2 = thread::spawn(move || {
///     for i in 0..1_000 {
///         f.check(&i);
///     }
/// });
///
/// handle1.join().unwrap();
/// handle2.join().unwrap();
/// ```
#[derive(Debug, Clone)]
pub struct SyncBloom<T> {
    inner: Arc<RwLock<Bloom<T>>>,
}

/// Pretty-print the filter.
///
/// # Examples
///
/// ```
/// use ofilter::SyncBloom;
///
/// let filter: SyncBloom<usize> = SyncBloom::new(100);
///
/// assert_eq!("[sync] { fp_rate: 0.000000, params: { nb_hash: 2, bit_len: 1899, nb_items: 100, fp_rate: 0.009992, predict: false } }", format!("{}", filter));
/// ```
impl<T> fmt::Display for SyncBloom<T>
where
    T: Hash,
{
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "[sync] {}", self.inner.read().unwrap())
    }
}

impl<T> SyncBloom<T> {
    /// Create a new thread-safe Bloom filter, with given capacity.
    ///
    /// All other parameters are set to defaults, or aligned to match capacity.
    ///
    /// # Examples
    ///
    /// ```
    /// use ofilter::SyncBloom;
    ///
    /// let filter: SyncBloom<usize> = SyncBloom::new(100);
    /// assert_eq!(100, filter.capacity());
    /// ```
    pub fn new(capacity: usize) -> Self {
        SyncBloom {
            inner: Arc::new(RwLock::new(Bloom::new(capacity))),
        }
    }

    /// Create a new thread-safe Bloom filter, with specific parameters.
    ///
    /// # Examples
    ///
    /// ```
    /// use ofilter::{SyncBloom, Params};
    ///
    /// let filter: SyncBloom<usize> = SyncBloom::new_with_params(Params::with_nb_items_and_fp_rate(100, 0.1));
    /// assert_eq!(100, filter.capacity());
    /// ```
    pub fn new_with_params(params: Params) -> Self {
        SyncBloom {
            inner: Arc::new(RwLock::new(Bloom::new_with_params(params))),
        }
    }

    /// Get filter params.
    ///
    /// This can be useful because when creating the filter, the `.adjust()`
    /// func is called, and may decide to fine-tune some parameters. With this,
    /// one can know exactly what is used by the filter.
    ///
    /// # Examples
    ///
    /// ```
    /// use ofilter::SyncBloom;
    ///
    /// let filter: SyncBloom<usize> = SyncBloom::new(100);
    /// println!("{}", filter.params());
    /// ```
    pub fn params(&self) -> Params {
        self.inner.read().unwrap().params()
    }

    /// Get filter capacity.
    ///
    /// Returns the value of `params.nb_items`, that is the number of
    /// items the filter is designed for.
    ///
    /// ```
    /// use ofilter::{SyncBloom, Params};
    ///
    /// let filter: SyncBloom<usize> = SyncBloom::new_with_params(Params::with_bit_len(1_000_000));
    /// assert_eq!(52681, filter.capacity());
    /// ```
    pub fn capacity(&self) -> usize {
        self.inner.read().unwrap().capacity()
    }

    /// Clear the filter.
    ///
    /// Clears the bit vector, but keeps parameters.
    ///
    /// ```
    /// use ofilter::SyncBloom;
    ///
    /// // Does not need to be `mut`
    /// let filter: SyncBloom<usize> = SyncBloom::new(1_000);
    /// filter.set(&10);
    /// assert!(filter.check(&10));
    /// filter.clear();
    /// assert!(!filter.check(&10));
    /// ```
    pub fn clear(&self) {
        self.inner.write().unwrap().clear();
    }

    /// Returns true if filter is empty.
    ///
    /// ```
    /// use ofilter::SyncBloom;
    ///
    /// // Does not need to be `mut`
    /// let filter: SyncBloom<usize> = SyncBloom::new(1_000);
    /// assert!(filter.is_empty());
    /// filter.set(&10);
    /// assert!(!filter.is_empty());
    /// ```
    pub fn is_empty(&self) -> bool {
        self.inner.read().unwrap().is_empty()
    }

    /// Returns the current false positive rate.
    ///
    /// In theory the false positive rate `fp_rate` is known at
    /// filter creation. But that, is the theoretical `fp_rate`
    /// that the filter reaches when it is "wasted" because it
    /// has too many entries. Until then, it performs better
    /// than that, statistically.
    ///
    /// This function returns the actual false positive rate.
    /// When this value is greater than the `fp_rate` from
    /// the parameters, one should not continue to add items
    /// to the filter.
    ///
    /// ```
    /// use ofilter::SyncBloom;
    ///
    /// // Does not need to be `mut`
    /// let filter: SyncBloom<usize> = SyncBloom::new(1_000);
    /// assert_eq!(0.0, filter.fp_rate());
    /// filter.set(&10);
    /// assert!(filter.fp_rate() > 0.0); // will be params.fp_rate when filter is full
    /// ```
    pub fn fp_rate(&self) -> f64 {
        self.inner.read().unwrap().fp_rate()
    }

    /// Returns the ratio between real fp_rate, and theoretical fp_rate.
    ///
    /// This is a helper to quickly compare the real `fp_rate`, given
    /// how filled the filter is, and the theoretical `fp_rate`.
    ///
    /// When this value is greater than 1.0, one should not
    /// continue to add items to the filter.
    ///
    /// ```
    /// use ofilter::SyncBloom;
    ///
    /// // Does not need to be `mut`
    /// let filter: SyncBloom<usize> = SyncBloom::new(1_000);
    /// assert_eq!(0.0, filter.level());
    /// filter.set(&10);
    /// assert!(filter.level() > 0.0); // will be 1.0 when filter is full
    /// ```
    pub fn level(&self) -> f64 {
        self.inner.read().unwrap().level()
    }

    /// Returns true if there is still room in the filter.
    ///
    /// This is a helper which returns true if the actual
    /// `fp_rate` is lower than the theoretical `fp_rate`.
    ///
    /// When this is false, one should not
    /// continue to add items to the filter.
    ///
    /// ```
    /// use ofilter::SyncBloom;
    ///
    /// // Does not need to be `mut`
    /// let filter: SyncBloom<usize> = SyncBloom::new(1_000);
    /// assert_eq!(0.0, filter.level());
    /// filter.set(&10);
    /// assert!(filter.level() > 0.0); // will be 1.0 when filter is full
    /// ```
    pub fn is_ok(&self) -> bool {
        self.inner.read().unwrap().is_ok()
    }

    /// Returns the number of ones in the bit vector.
    ///
    /// By comparing this number with the size of the bit vector,
    /// one can estimate how "filled" the filter is.
    ///
    /// ```
    /// use ofilter::SyncBloom;
    ///
    /// // Does not need to be `mut`
    /// let filter: SyncBloom<usize> = SyncBloom::new(1_000_000);
    /// assert_eq!(0, filter.count_ones());
    /// filter.set(&10);
    /// assert_eq!(2, filter.count_ones());
    /// ```
    pub fn count_ones(&self) -> usize {
        self.inner.read().unwrap().count_ones()
    }

    /// Export the underlying bit vector.
    ///
    /// ```
    /// use ofilter::SyncBloom;
    ///
    /// // Does not need to be `mut`
    /// let filter: SyncBloom<usize> = SyncBloom::new(1_000_000);
    /// let bit_vec = filter.export();
    /// println!("{:?}", &bit_vec);
    /// ```
    pub fn export(&self) -> BitVec {
        self.inner.read().unwrap().export().clone()
    }

    /// Import the underlying bit vector.
    ///
    /// ```
    /// use ofilter::SyncBloom;
    /// use bitvec::prelude::BitVec;
    ///
    /// // Does not need to be `mut`
    /// let filter: SyncBloom<usize> = SyncBloom::new(1_000_000);
    /// let mut bit_vec = BitVec::new();
    /// bit_vec.resize(100, false);
    /// bit_vec.set(42, true);
    /// filter.import(&bit_vec); // safe to call even is sizes mismatch
    /// ```
    pub fn import(&self, src: &BitVec) {
        self.inner.write().unwrap().import(src);
    }
}

impl<T> SyncBloom<T>
where
    T: Hash,
{
    /// Record an item in the set.
    ///
    /// Once this has been called, any call to `check()` will return
    /// true, as there are no false negatives. However some other items
    /// may test positive as a consequence of recording this one.
    ///
    /// ```
    /// use ofilter::SyncBloom;
    ///
    /// // Does not need to be `mut`
    /// let filter: SyncBloom<usize> = SyncBloom::new(1_000);
    /// filter.set(&42);
    /// ```
    pub fn set(&self, item: &T) {
        self.inner.write().unwrap().set(item);
    }

    /// Guess whether an item is likely to be in the set.
    ///
    /// If `set()` has been called before with value, then this returns
    /// true, as there are no false negatives. However it may respond
    /// true even if the item has never been recorded in the set.
    ///
    /// ```
    /// use ofilter::SyncBloom;
    ///
    /// // Does not need to be `mut`
    /// let filter: SyncBloom<usize> = SyncBloom::new(1_000);
    /// filter.set(&42);
    /// assert!(filter.check(&42));
    /// ```
    pub fn check(&self, item: &T) -> bool {
        self.inner.read().unwrap().check(item)
    }

    /// Record an item in the set and returns its previous value.
    ///
    /// Equivalent to calling `get()` then `set()` but performs
    /// hash lookup only once so it's a bit more efficient.
    ///
    /// ```
    /// use ofilter::SyncBloom;
    ///
    /// // Does not need to be `mut`
    /// let filter: SyncBloom<usize> = SyncBloom::new(1_000);
    /// assert!(!filter.check_and_set(&42));
    /// assert!(filter.check(&42));
    /// ```
    pub fn check_and_set(&self, item: &T) -> bool {
        self.inner.write().unwrap().check_and_set(item)
    }
}
