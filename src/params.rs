// Copyright (C) 2024 Christian Mauduit <ufoot@ufoot.org>

use std::fmt;

/// Parameters used to build bloom filters.
///
/// This is a utility to set up various bloom filter parameters.
/// There are typically 4 parameters:
///
/// - n (`nb_items`): number of items in the filter
/// - p (`fp_rate`): probability of false positives, fraction between 0 and 1
/// - m (`bit_len`): number of bits in the filter
/// - k (`nb_hash`): number of hash functions
///
/// All of them are linked, if one changes the size of the filter, obviously,
/// it changes the number of items it can hold.
///
/// A few things to keep in mind:
/// - for a pure Bloom filter, the only technical parameters that matter
///   are `nb_hash` and `bit_len`. They fully define the filter.
/// - from a functional point of view, most of the time you are interested
///   in a given `nb_items` and `fp_rate`. From there the 2 others are derived.
///
/// You may partially fill this struct, anything which contains `zero` will
/// be inferred either by deducing it from other defined params, or by
/// replacing it with a default value. This is done by calling `.adjust()`.
///
/// Also, there is a `predict` field which can be used to turn the
/// filter into a predicatable filter. This is convenient for testing,
/// and may be of interest in edge cases but most of the time a real random
/// version is prefered. It avoids bias and also can protect against
/// some DDOS attacks based on hash collision.
///
/// A few helpers are defined as well, for common use-cases.
///
/// # A bit of theory
///
/// This [interactive Bloom filter params calulator](https://hur.st/bloomfilter/?n=300000&p=0.01&m=&k=)
/// proved very useful while developping this.
/// Also it recaps the usual formulas, with:
///
/// * n -> number of items in the filter
/// * p -> probability of false positives, fraction between 0 and 1
/// * m -> number of bits in the filter
/// * k -> number of hash functions
///
/// We have:
///
/// * `n = ceil(m / (-k / log(1 - exp(log(p) / k))))`
/// * `p = pow(1 - exp(-k / (m / n)), k)`
/// * `m = ceil((n * log(p)) / log(1 / pow(2, log(2))))`
/// * `k = round((m / n) * log(2))`
///
/// A command line equivalent would be this practical
/// [bloom-filter-calculator.rb gist](https://gist.github.com/brandt/8f9ab3ceae37562a2841)
///
/// Note that there are corner cases, for example, the formula that gives
/// `bit_len` (m) from `nb_items` (n) and `fp_rate` (p), corresponds to
/// the optimal case, when `nb_hash` (k) has been chosen optimally. If not,
/// it has to be revisited and adapted to the real value of `nb_hash` (k).
/// In practice, unless you impose it, what this package does is enforce
/// a `nb_hash` (k) of 2, which is generally optimal if you consider CPU time
/// and not memory usage.
///
/// # Links
///
/// * [Bloom Filter on Wikipedia](https://en.wikipedia.org/wiki/Bloom_filter)
/// * [All About Bloom Filters](https://freecontent.manning.com/all-about-bloom-filters/)
/// * [Buffered Bloom Filters on Solid State Storage](https://www.vldb.org/archives/workshop/2010/proceedings/files/vldb_2010_workshop/ADMS_2010/adms10-canim.pdf)
/// * [Age-Partitioned Bloom Filters](https://arxiv.org/pdf/2001.03147.pdf)
/// * [countBF: A General-purpose High Accuracy and Space Efficient Counting Bloom Filter](https://arxiv.org/pdf/2106.04364.pdf)
/// * [Stable Learned Bloom Filters for Data Streams](https://www.vldb.org/pvldb/vol13/p2355-liu.pdf)
/// * [Building a Better Bloom Filter](https://www.eecs.harvard.edu/~michaelm/postscripts/tr-02-05.pdf)
///
/// # Examples
///
/// Getting a filter for a given number of items, everything else default:
///
/// ```
/// use ofilter::Params;
///
/// let params = Params::with_nb_items(10_000);
///
/// assert_eq!("{ nb_hash: 2, bit_len: 189825, nb_items: 10000, fp_rate: 0.010000, predict: false }", format!("{}", &params));
/// ```
///
/// Getting a filter for a given number of items, false positive rate, and enforcing number of hash:
///
/// ```
/// use ofilter::Params;
///
/// let params = Params{
///     nb_hash: 3,
///     bit_len: 0,
///     nb_items: 100_000,
///     fp_rate: 0.1,
///     predict: false,
/// }.adjust();
///
/// assert_eq!("{ nb_hash: 3, bit_len: 480833, nb_items: 100000, fp_rate: 0.100000, predict: false }", format!("{}", &params));
/// ```
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
#[derive(Debug, Clone)]
pub struct Params {
    /// Number of hash functions used by the filter.
    ///
    /// Also referred to as `k` is most Bloom filter papers.
    pub nb_hash: usize,
    /// Length of the bit vector used by the filter.
    ///
    /// Also referred to as `m` is most Bloom filter papers.
    pub bit_len: usize,
    /// Number of items the Bloom filter is designed for.
    ///
    /// Also referred to as `n` is most Bloom filter papers.
    pub nb_items: usize,
    /// False positive rate of the Bloom filter.
    ///
    /// Also referred to as `p` is most Bloom filter papers.
    pub fp_rate: f64,
    /// If set to true, Bloom filter is predictable.
    ///
    /// Use this for testing, when you want something that
    /// is 100% predictable and avoid flaky behavior.
    /// In production, it would be safer to rely on the
    /// random, statistical default behavior.
    ///
    /// One reason is security, among other examples, using
    /// a predictable hash for a cache may expose you to
    /// some sort of DDOS attack.
    ///
    /// If in doubt, leave it to false.
    pub predict: bool,
}

/// Compare two parameters sets.
///
/// The false positive rate is rounded to 6 digits.
///
/// # Examples
///
/// ```
/// use ofilter::Params;
///
/// let p1 = Params::with_nb_items(100);
/// let mut p2 = Params::with_nb_items(100);
/// p2.fp_rate += 1e-8;
/// assert_eq!(p1, p2);
/// ```
impl std::cmp::PartialEq for Params {
    fn eq(&self, other: &Self) -> bool {
        if self.nb_hash != other.nb_hash {
            return false;
        }
        if self.bit_len != other.bit_len {
            return false;
        }
        if self.nb_items != other.nb_items {
            return false;
        }
        ((self.fp_rate * 1e6).round() as isize) == ((other.fp_rate * 1e6).round() as isize)
    }
}

impl std::cmp::Eq for Params {}

/// Pretty-print parameters
///
/// # Examples
///
/// ```
/// use ofilter::Params;
///
/// let p = Params::with_nb_items(100);
/// assert_eq!("{ nb_hash: 2, bit_len: 1899, nb_items: 100, fp_rate: 0.009992, predict: false }", format!("{}", p));
/// ```
impl fmt::Display for Params {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "{{ nb_hash: {}, bit_len: {}, nb_items: {}, fp_rate: {:0.6}, predict: {} }}",
            self.nb_hash, self.bit_len, self.nb_items, self.fp_rate, self.predict
        )
    }
}

/// Default number of hash functions.
///
/// We use 2 because most of the time, the game changer is CPU time,
/// and limiting the number of hashes makes things quite optimal from
/// that point of view. It is only a default, feel free to increase
/// when relevant, but this proved a quite sensible setting.
pub const DEFAULT_NB_HASH: usize = 2;

/// Default size of the bit vector, in bits.
///
/// This value looks pretty random and corresponds to the required
/// buffer len to power a 1000 items filter with 1% accuracy.
/// Generally speaking, do not impose the buffer len, it is safe
/// to derive it from other more functional aspects such as number
/// of logical items or false positive rate.
pub const DEFAULT_BIT_LEN: usize = 18_983;

/// Default number of items the filter should contain.
///
/// Think of this as a maximum capacity, above this, the filter
/// stops honoring the false positive rate and ultimately,
/// every item will test positive.
pub const DEFAULT_NB_ITEMS: usize = 1_000;

/// Default false positive rate.
///
/// Bloom filters, by design, have false positives. This is the
/// default rate, feel free to fine-tune it. But beware that
/// lowering this too much can yield very big filters with
/// a huge bit buffer. It's a tradeoff.
pub const DEFAULT_FALSE_P: f64 = 0.01;

const MIN_FALSE_P: f64 = 0.000_000_001;
const MAX_FALSE_P: f64 = 0.999_999_999;

impl Params {
    /// Parameters to store a given number of items.
    ///
    /// All other values are using defaults, or are derive from this.
    /// This is the go-to function to create a reasonable filter that
    /// holds N items.
    ///
    /// # Examples:
    ///
    /// ```
    /// use ofilter::Params;
    ///
    /// let p = Params::with_nb_items(10_000);
    /// assert_eq!("{ nb_hash: 2, bit_len: 189825, nb_items: 10000, fp_rate: 0.010000, predict: false }", format!("{}", p));
    /// ```
    pub fn with_nb_items(nb_items: usize) -> Params {
        Params {
            nb_hash: DEFAULT_NB_HASH,
            bit_len: 0,
            nb_items,
            fp_rate: DEFAULT_FALSE_P,
            predict: false,
        }
        .adjust()
    }

    /// Parameters to store a given number of items, enforcing false positive rate.
    ///
    /// By defining `nb_items` and `fp_rate`, the filter is almost completely defined.
    /// The other missing parameter is the number of hash functions. There is an
    /// optimal number for this, but this function will enforce a default of 2,
    /// which most of the time yields excellent results, and avoids calculating
    /// too many hashes.
    ///
    /// If you want to use the optimal number of hashes, create the Params struct
    /// with `nb_items`, `fp_rate` and `nb_hash` defined, then call `.adjust()`.
    ///
    /// # Examples:
    ///
    /// ```
    /// use ofilter::Params;
    ///
    /// let p = Params::with_nb_items_and_fp_rate(10_000, 0.001);
    /// assert_eq!("{ nb_hash: 2, bit_len: 622402, nb_items: 10000, fp_rate: 0.001000, predict: false }", format!("{}", p));
    /// ```
    pub fn with_nb_items_and_fp_rate(nb_items: usize, fp_rate: f64) -> Params {
        Params {
            nb_hash: DEFAULT_NB_HASH,
            bit_len: 0,
            nb_items,
            fp_rate,
            predict: false,
        }
        .adjust()
    }

    /// Parameters to guess the number of items from bit buffer size.
    ///
    /// If the only thing you care about, this will give you the best
    /// result for that amount of memory.
    ///
    /// # Examples:
    ///
    /// ```
    /// use ofilter::Params;
    ///
    /// let p = Params::with_bit_len(8_192);
    /// assert_eq!("{ nb_hash: 2, bit_len: 8192, nb_items: 432, fp_rate: 0.010019, predict: false }", format!("{}", p));
    /// ```
    pub fn with_bit_len(bit_len: usize) -> Params {
        Params {
            nb_hash: DEFAULT_NB_HASH,
            bit_len,
            nb_items: 0,
            fp_rate: DEFAULT_FALSE_P,
            predict: false,
        }
        .adjust()
    }

    /// Parameters to store a given number of items, enforcing bit buffer size
    ///
    /// If the only thing you care about, this will give you the best
    /// result for that amount of memory.
    ///
    /// # Examples:
    ///
    /// ```
    /// use ofilter::Params;
    ///
    /// let p = Params::with_bit_len_and_nb_items(8_192, 1_000);
    /// assert_eq!("{ nb_hash: 2, bit_len: 8192, nb_items: 1000, fp_rate: 0.046925, predict: false }", format!("{}", p));
    /// ```
    pub fn with_bit_len_and_nb_items(bit_len: usize, nb_items: usize) -> Params {
        Params {
            nb_hash: DEFAULT_NB_HASH,
            bit_len,
            nb_items,
            fp_rate: 0.0,
            predict: false,
        }
        .adjust()
    }

    /// Estimate the number of items one can store.
    ///
    /// This applies the formula `n = ceil(m / (-k / log(1 - exp(log(p) / k))))`.   
    ///
    /// # Examples:
    ///
    /// ```
    /// use ofilter::Params;
    ///
    /// assert_eq!(1994, Params::estimate_nb_items(5, 10_000, 0.1));
    /// ```
    pub fn estimate_nb_items(nb_hash: usize, bit_len: usize, fp_rate: f64) -> usize {
        // n = ceil(m / (-k / log(1 - exp(log(p) / k))))
        std::cmp::max(
            (bit_len as f64
                / (-(nb_hash as f64) / (1.0 - (fp_rate.ln() / nb_hash as f64).exp()).ln()))
            .ceil() as usize,
            1,
        )
    }

    /// Estimate the false positive rate.
    ///
    /// This applies the formula `p = powf(1 - exp(-k / (m / n)), k)`
    ///
    /// # Examples:
    ///
    /// ```
    /// use ofilter::Params;
    ///
    /// assert_eq!("0.009431", format!("{:0.6}", Params::estimate_fp_rate(5, 10_000, 1_000)));
    /// ```
    pub fn estimate_fp_rate(nb_hash: usize, bit_len: usize, nb_items: usize) -> f64 {
        // p = powf(1 - exp(-k / (m / n)), k)
        (1.0 - (-(nb_hash as f64) / (bit_len as f64 / nb_items as f64)).exp()).powf(nb_hash as f64)
    }

    /// Calculate the optimal number of bits.
    ///
    /// This applies the formula `m = ceil((n * log(p)) / log(1 / pow(2, log(2))))`
    ///
    /// Note that this is the *optimal* number of bits, but for the case
    /// where the number of hash has also been chosen optimally. You'll notice
    /// the formula does not depend on `fp_rate` (p).
    ///
    /// See `guess_bit_len_for_fp_rate` if you want to also take `fp_rate` in account.
    ///
    /// # Examples:
    ///
    /// ```
    /// use ofilter::Params;
    ///
    /// assert_eq!(62353, Params::optimal_bit_len(10_000, 0.05));
    /// ```
    pub fn optimal_bit_len(nb_items: usize, fp_rate: f64) -> usize {
        // m = ceil((n * log(p)) / log(1 / pow(2, log(2))));
        std::cmp::max(
            ((nb_items as f64 * fp_rate.ln()) / (1.0 / 2f64.powf(2f64.ln())).ln()).ceil() as usize,
            1,
        )
    }

    /// Calculate the optimal number of hash.
    ///
    /// This applies the formula `k = round((m / n) * log(2))`
    ///
    /// Note that this is the *optimal* number of hash, but it does not
    /// account for false positive rate, which you may want to impose.
    /// You'll notice the formula does not depend on `fp_rate` (p).
    ///
    /// See `guess_bit_len_for_fp_rate` if you want to also take `fp_rate` in account.
    ///
    /// # Examples:
    ///
    /// ```
    /// use ofilter::Params;
    ///
    /// assert_eq!(13, Params::optimal_nb_hash(10_000, 500));
    /// ```
    pub fn optimal_nb_hash(bit_len: usize, nb_items: usize) -> usize {
        // k = round((m / n) * log(2));
        std::cmp::max(
            ((bit_len as f64 / nb_items as f64) * 2f64.ln()).floor() as usize,
            1,
        )
    }

    /// Guess the number of bits for a given false positive rate.
    ///
    /// There is a formula `m = ceil((n * log(p)) / log(1 / pow(2, log(2))))` to get this
    /// result but it gives it for the optimal case. Sometimes you can
    /// prefer to, typically, lower the number of bits in the bits buffer to
    /// get something smaller in memory, and just give up a bit on the optimal
    /// `fp_rate` (p).
    ///
    /// This function does a bisect search to find the correct value.
    ///
    /// # Examples:
    ///
    /// ```
    /// use ofilter::Params;
    ///
    /// assert_eq!(480833, Params::guess_bit_len_for_fp_rate(3, 100_000, 0.1));
    /// ```
    pub fn guess_bit_len_for_fp_rate(nb_hash: usize, nb_items: usize, fp_rate: f64) -> usize {
        // Doing a bisect to find the point where fp_rate is exactly what we want,
        // knowing that fp_rate decreases as nb_items increases
        let mut bit_len_hi = usize::MAX / 2 - 1; // to avoid out-of-bounds when summing
        let mut bit_len_lo = std::cmp::min(Self::optimal_bit_len(nb_items, fp_rate), bit_len_hi);
        let fp_rate = Self::adjust_fp_rate(fp_rate);

        while bit_len_hi > bit_len_lo + 1 {
            let fp_rate_lo = Self::estimate_fp_rate(nb_hash, bit_len_lo, nb_items);
            if fp_rate_lo < fp_rate {
                return bit_len_lo;
            }
            let bit_len_mi = (bit_len_lo + bit_len_hi) / 2;
            let fp_rate_mi = Self::estimate_fp_rate(nb_hash, bit_len_mi, nb_items);
            if fp_rate_mi <= fp_rate {
                bit_len_hi = bit_len_mi;
            } else {
                bit_len_lo = bit_len_mi;
            }
        }

        bit_len_hi
    }

    /// Guess the number of hash for a given false positive rate.
    ///
    /// There is a formula `k = round((m / n) * log(2))` to get this
    /// result but it gives it for the optimal case. Sometimes you can
    /// prefer to, typically, lower the number of hash functions to
    /// get something faster, and just give up a bit on the optimal
    /// `fp_rate` (p).
    ///
    /// This function starts with the optimal `nb_hash` as a seed,
    /// and then tries to lower the it as long as the false positive rate
    /// gets closer to the target `fp_rate`.
    ///
    /// # Examples:
    ///
    /// ```
    /// use ofilter::Params;
    ///
    /// assert_eq!(6, Params::guess_nb_hash_for_fp_rate(100_000, 10_000, 0.0005));
    /// ```
    pub fn guess_nb_hash_for_fp_rate(bit_len: usize, nb_items: usize, fp_rate: f64) -> usize {
        let mut best_diff = Self::estimate_fp_rate(DEFAULT_NB_HASH, bit_len, nb_items);
        let mut best_nb_hash = DEFAULT_NB_HASH;

        let mut tmp_nb_hash = Self::optimal_nb_hash(bit_len, nb_items);
        while tmp_nb_hash > 0 {
            let diff = (Self::estimate_fp_rate(tmp_nb_hash, bit_len, nb_items) - fp_rate).abs();
            if diff < best_diff {
                best_diff = diff;
                best_nb_hash = tmp_nb_hash;
            }
            tmp_nb_hash -= 1;
        }

        best_nb_hash
    }

    fn adjust_fp_rate(fp_rate: f64) -> f64 {
        if fp_rate == 0.0 {
            0.0
        } else if fp_rate <= MIN_FALSE_P {
            MIN_FALSE_P
        } else if fp_rate >= MAX_FALSE_P {
            MAX_FALSE_P
        } else {
            fp_rate
        }
    }

    /// Adjust params so that they are consistent.
    ///
    /// The 4 parameters in a Bloom filter are closely linked.
    /// More precisely, with:
    ///
    /// * n (`nb_items`) -> number of items in the filter
    /// * p (`fp_rate`) -> probability of false positives, fraction between 0 and 1
    /// * m (`bit_len`) -> number of bits in the filter
    /// * k (`nb_hash`) -> number of hash functions
    ///
    /// We have:
    ///
    /// * `n = ceil(m / (-k / log(1 - exp(log(p) / k))))`
    /// * `p = pow(1 - exp(-k / (m / n)), k)`
    /// * `m = ceil((n * log(p)) / log(1 / pow(2, log(2))))`
    /// * `k = round((m / n) * log(2))`
    ///
    /// This function works as follows:
    /// - if any parameter is non-zero, tries to respect it
    /// - if any parameter is zero, and can be inferred from others, calculate it
    /// - for all other parameters, use defaults
    ///
    /// Once everything has a value, it will ultimately re-calculate the `fp_rate`
    /// so that it matches the other 3, which are integers, so may not be changed
    /// linearily.
    ///
    /// In practice, use it to feed the `new_with_params` constructors for filters.
    ///
    /// # Examples:
    ///
    /// ```
    /// use ofilter::Params;
    ///
    /// let params = Params{
    ///     nb_hash: 2,
    ///     bit_len: 0,
    ///     nb_items: 1_000,
    ///     fp_rate: 0.1,
    ///     predict: false,
    /// };
    ///
    /// assert_eq!("{ nb_hash: 2, bit_len: 0, nb_items: 1000, fp_rate: 0.100000, predict: false }",
    ///     format!("{}", params)
    /// );
    /// assert_eq!("{ nb_hash: 2, bit_len: 5262, nb_items: 1000, fp_rate: 0.099980, predict: false }",
    ///     format!("{}", params.adjust())
    /// );
    /// ```
    pub fn adjust(self) -> Self {
        let fp_rate = Self::adjust_fp_rate(self.fp_rate);
        // bit_len + nb_items -> nb_hash              (optimal, guess)
        // nb_hash + bit_len + fp_rate -> nb_items    (exact, no choice)
        // nb_items + fp_rate -> bit_len              (optimal, guess)
        // nb_hash + bit_len + nb_items -> fp_rate    (exact, no choice)

        // Everything is defined, just decide what we're going to
        // expose in terms of "this filter should be capable of that".
        if self.nb_hash > 0 && self.bit_len > 0 {
            let fp_rate = if fp_rate == 0.0 {
                DEFAULT_FALSE_P
            } else {
                fp_rate
            };
            let nb_items = match self.nb_items {
                0 => Self::estimate_nb_items(self.nb_hash, self.bit_len, fp_rate),
                nb_items => nb_items,
            };
            // Adjust, always, the fp_rate, as it's a float altering it a bit
            // is never so surprising, it's understandable that is has to
            // be rounded to match the other values.
            return Params {
                nb_hash: self.nb_hash,
                bit_len: self.bit_len,
                nb_items,
                fp_rate: Self::estimate_fp_rate(self.nb_hash, self.bit_len, nb_items),
                predict: self.predict,
            };
            // The only output on which we do not call adjust()
        };

        // Functionnally, the filter is fully defined by
        // number of items and false positives, derive tech details.
        if self.nb_items > 0 && fp_rate > 0.0 {
            if self.nb_hash == 0 && self.bit_len > 0 {
                return Params {
                    // Too many constraints, what we do here is find the
                    // right number of hashes to satisfy best the constraints
                    // on false positives. Sometimes there is litterally no
                    // way to do it, sometimes lowering the number of hash
                    // may lower the CPU for a result which is good enough.
                    nb_hash: Self::guess_nb_hash_for_fp_rate(self.bit_len, self.nb_items, fp_rate),
                    bit_len: self.bit_len,
                    nb_items: self.nb_items,
                    fp_rate,
                    predict: self.predict,
                }
                .adjust();
            };
            if self.nb_hash > 0 && self.bit_len == 0 {
                return Params {
                    nb_hash: self.nb_hash,
                    bit_len: Self::guess_bit_len_for_fp_rate(self.nb_hash, self.nb_items, fp_rate),
                    nb_items: self.nb_items,
                    fp_rate,
                    predict: self.predict,
                }
                .adjust();
            }
        }

        // The filter is partially defined, start by defining the
        // number of hash, because it's performance impacting, and
        // by default a small number of hash makes it faster.
        if self.nb_hash == 0 {
            let nb_hash = if self.bit_len > 0 && self.nb_items > 0 {
                Self::optimal_nb_hash(self.bit_len, self.nb_items)
            } else {
                DEFAULT_NB_HASH
            };
            return Params {
                nb_hash,
                bit_len: self.bit_len,
                nb_items: self.nb_items,
                fp_rate,
                predict: self.predict,
            }
            .adjust();
        };

        // If no choice could be made so far, define default
        // functional requirements and see if we can do better.
        if fp_rate == 0.0 {
            return Params {
                nb_hash: self.nb_hash,
                bit_len: self.bit_len,
                nb_items: self.nb_items,
                fp_rate: DEFAULT_FALSE_P,
                predict: self.predict,
            }
            .adjust();
        };

        if self.nb_items == 0 {
            return Params {
                nb_hash: self.nb_hash,
                bit_len: self.bit_len,
                nb_items: DEFAULT_NB_ITEMS,
                fp_rate,
                predict: self.predict,
            }
            .adjust();
        };

        // This should be unreachable, but keeping it as a catch all.
        if self.bit_len == 0 {
            return Params {
                nb_hash: self.nb_hash,
                bit_len: DEFAULT_BIT_LEN,
                nb_items: self.nb_items,
                fp_rate,
                predict: self.predict,
            }
            .adjust();
        };

        // No clue what to do, pure defaults...
        Self::default()
    }
}

impl std::default::Default for Params {
    fn default() -> Self {
        Params {
            nb_hash: DEFAULT_NB_HASH,
            bit_len: DEFAULT_BIT_LEN,
            nb_items: DEFAULT_NB_ITEMS,
            fp_rate: DEFAULT_FALSE_P,
            predict: false,
        }
        .adjust()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_default() {
        assert_eq!(Params::default(), Params::default().adjust());
    }

    #[test]
    fn test_nb_items() {
        assert_eq!(17, Params::estimate_nb_items(2, 1_000, 0.001));
    }

    #[test]
    fn test_fp_rate() {
        assert_eq!(
            32_859,
            (Params::estimate_fp_rate(2, 1_000, 100) * 1e6).round() as usize
        );
    }

    #[test]
    fn test_optimal_bit_len() {
        assert_eq!(1438, Params::optimal_bit_len(100, 0.001));
    }

    #[test]
    fn test_optimal_nb_hash() {
        assert_eq!(69, Params::optimal_nb_hash(10_000, 100));
    }

    #[test]
    fn test_params_default() {
        assert_eq!(
            "{ nb_hash: 2, bit_len: 18983, nb_items: 1000, fp_rate: 0.009999, predict: false }",
            format!("{}", Params::default())
        );
    }

    #[test]
    fn test_params_adjust() {
        let params = Params {
            nb_hash: 0,
            bit_len: 0,
            nb_items: 1_000_000,
            fp_rate: 0.001,
            predict: false,
        };
        assert_eq!(
           "{ nb_hash: 2, bit_len: 62240198, nb_items: 1000000, fp_rate: 0.001000, predict: false }",
            format!("{}", params.adjust())
        );

        let params = Params {
            nb_hash: 4,
            bit_len: 0,
            nb_items: 1_000_000,
            fp_rate: 0.1,
            predict: false,
        };
        assert_eq!(
            "{ nb_hash: 4, bit_len: 4840764, nb_items: 1000000, fp_rate: 0.100000, predict: false }",
            format!("{}", params.adjust())
        );

        let params = Params {
            nb_hash: 0,
            bit_len: 10_000_000,
            nb_items: 1_000_000,
            fp_rate: 0.0001,
            predict: false,
        };
        assert_eq!(
            "{ nb_hash: 6, bit_len: 10000000, nb_items: 1000000, fp_rate: 0.008436, predict: false }",
            format!("{}", params.adjust())
        );

        let params = Params {
            nb_hash: 0,
            bit_len: 10_000_000,
            nb_items: 1_000_000,
            fp_rate: 0.1,
            predict: false,
        };
        assert_eq!(
            "{ nb_hash: 1, bit_len: 10000000, nb_items: 1000000, fp_rate: 0.095163, predict: false }",
            format!("{}", params.adjust())
        );

        let params = Params {
            nb_hash: 2,
            bit_len: 0,
            nb_items: 1_000_000,
            fp_rate: 0.00001,
            predict: false,
        };
        assert_eq!(
            "{ nb_hash: 2, bit_len: 631455005, nb_items: 1000000, fp_rate: 0.000010, predict: false }",
            format!("{}", params.adjust())
        );

        let params = Params {
            nb_hash: 2,
            bit_len: 0,
            nb_items: 1_000_000,
            fp_rate: 0.1,
            predict: false,
        };
        assert_eq!(
            "{ nb_hash: 2, bit_len: 5261353, nb_items: 1000000, fp_rate: 0.100000, predict: false }",
            format!("{}", params.adjust())
        );
    }
}
