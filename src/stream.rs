// Copyright (C) 2024 Christian Mauduit <ufoot@ufoot.org>

use crate::bloom::Bloom;
use crate::params::Params;
use std::fmt;
use std::hash::Hash;

/// Streaming Bloom filter.
///
/// This is based on a pair of Bloom filters, and is designed to
/// serve as a "streaming" filter. What does this mean?
///
/// In a regular Bloom filter, after "some time" which corresponds
/// to `nb_items` different items set info the filter, the filter
/// stops working well.
///
/// In this implementation, we use a pair of Bloom filters, once
/// they are "wasted" because they have too many items, we drop
/// one of them, and only keep the other. This way, we keep
/// a trace of what was in the filter, but avoid growing
/// infinitely and reach a state where any item would pass the filter.
///
/// In practice it is much more imprecise than a regular Bloom
/// filter and the false positive rate is quite high. However
/// it never reaches the point where it is totally unusable,
/// it always refuses some items, and keeps the set within a
/// reasonable limit.
///
/// # Examples
///
/// ```
/// use ofilter::Stream;
/// use std::hash::Hash;
///
/// #[derive(Hash)]
/// struct Obj {
///     some: usize,
///     thing: usize,
/// }
///
/// let mut filter: Stream<Obj> = Stream::new(100);
/// let obj = Obj{ some: 1, thing: 2};
///
/// assert!(!filter.check(&obj)); // object is not in filter
/// filter.set(&obj);             // put object in filter
/// assert!(filter.check(&obj));  // object is in filter
///
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
#[derive(Debug, Clone)]
pub struct Stream<T> {
    params: Params,
    orig_params: Params,
    check_period: usize,
    nb_sets: usize,
    nb_resets: usize,
    odd: Bloom<T>,
    even: Bloom<T>,
}

/// Pretty-print the filter.
///
/// # Examples
///
/// ```
/// use ofilter::Stream;
///
/// let filter: Stream<usize> = Stream::new(100);
///
/// assert_eq!("{ age: 0, fp_rates: (0.000000, 0.000000), params: { nb_hash: 2, bit_len: 1899, nb_items: 100, fp_rate: 0.009992, predict: false } }" , format!("{}", filter));
/// ```
impl<T> fmt::Display for Stream<T> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let fp_rates = self.fp_rates();
        write!(
            f,
            "{{ age: {}, fp_rates: ({:0.6}, {:0.6}), params: {} }}",
            self.age(),
            fp_rates.0,
            fp_rates.1,
            self.params
        )
    }
}

impl<T> Stream<T> {
    /// Create a new streaming Bloom filter, with given capacity.
    ///
    /// All other parameters are set to defaults, or aligned to match capacity.
    ///
    /// This is different from a classic Bloom filter, it maintains a set
    /// of items, in this set you can be sure that the last `capacity` items
    /// are always kept in the filter. In other words, it works like a Bloom
    /// filter for the most recent items. Ancient items are progressively
    /// removed from the set.
    ///
    /// # Examples
    ///
    /// ```
    /// use ofilter::Stream;
    ///
    /// let filter: Stream<usize> = Stream::new(100);
    /// assert_eq!(100, filter.capacity());
    /// ```
    pub fn new(capacity: usize) -> Self {
        let capacity = std::cmp::max(1, capacity);
        Self::new_with_params(Params::with_nb_items(capacity))
    }

    /// Create a new streaming Bloom filter, with specific parameters.
    ///
    /// # Examples
    ///
    /// ```
    /// use ofilter::{Stream, Params};
    ///
    /// let filter: Stream<usize> = Stream::new_with_params(Params::with_nb_items_and_fp_rate(100, 0.1));
    /// assert_eq!(100, filter.capacity());
    /// ```
    pub fn new_with_params(params: Params) -> Self {
        let params = params.adjust();
        let mut stream = Stream {
            params: params.clone(),
            orig_params: params.clone(),
            check_period: 0,
            nb_sets: 0,
            nb_resets: 0,
            odd: Bloom::new_with_params(params.clone()),
            even: Bloom::new_with_params(params.clone()),
        };
        stream.update_check_period();
        stream
    }

    fn update_check_period(&mut self) {
        let check_period = std::cmp::max(
            1,
            ((self.params.nb_items as f64) * self.params.fp_rate) as usize,
        );
        self.check_period = check_period;
    }

    /// Get filter params.
    ///
    /// This can be useful because when creating the filter, the `.adjust()`
    /// func is called, and may decide to fine-tune some parameters. With this,
    /// one can know exactly what is used by the filter.
    ///
    /// # Examples
    ///
    /// ```
    /// use ofilter::Stream;
    ///
    /// let filter: Stream<usize> = Stream::new(100);
    /// println!("{}", filter.params());
    /// ```
    pub fn params(&self) -> Params {
        self.params.clone()
    }

    /// Get filter capacity.
    ///
    /// Returns the value of `params.nb_items`, that is the number of
    /// items the filter is designed for.
    ///
    /// ```
    /// use ofilter::{Stream, Params};
    ///
    /// let filter: Stream<usize> = Stream::new_with_params(Params::with_bit_len(1_000_000));
    /// assert_eq!(52681, filter.capacity());
    /// ```
    pub fn capacity(&self) -> usize {
        self.params.nb_items
    }

    /// Clear the filter.
    ///
    /// Clears the bit vector, but keeps parameters.
    ///
    /// ```
    /// use ofilter::Stream;
    ///
    /// let mut filter: Stream<usize> = Stream::new(1_000);
    /// filter.set(&10);
    /// assert!(filter.check(&10));
    /// filter.clear();
    /// assert!(!filter.check(&10));
    /// ```
    pub fn clear(&mut self) {
        // Create new filters instead of just clearing them because
        // params may have changed and we want to use new ones.
        self.odd = Bloom::new_with_params(self.params.clone());
        self.even = Bloom::new_with_params(self.params.clone());
    }

    /// Returns true if filter is empty.
    ///
    /// ```
    /// use ofilter::Stream;
    ///
    /// let mut filter: Stream<usize> = Stream::new(1_000);
    /// assert!(filter.is_empty());
    /// filter.set(&10);
    /// assert!(!filter.is_empty());
    /// ```
    pub fn is_empty(&self) -> bool {
        self.odd.is_empty() && self.even.is_empty()
    }

    /// Returns the current max false positive rate.
    ///
    /// In theory the false positive rate `fp_rate` is known at
    /// filter creation. But that, is the theoretical `fp_rate`
    /// that the filter reaches when it is "wasted" because it
    /// has too many entries. Until then, it performs better
    /// than that, statistically.
    ///
    /// As this filter uses two filters under the hood, there
    /// are technically 2 values for the actual false positive rate.
    ///
    /// This function returns the max value, which is usually
    /// the most significant,
    /// as it's reflecting the current behavior of the filter.
    ///
    /// ```
    /// use ofilter::Stream;
    ///
    /// let mut filter: Stream<usize> = Stream::new(1_000);
    /// assert_eq!(0.0, filter.max_fp_rate());
    /// filter.set(&10);
    /// assert!(filter.max_fp_rate() > 0.0); // will be params.fp_rate when filter is full
    /// ```
    pub fn max_fp_rate(&self) -> f64 {
        let odd_fp_rate = self.odd.fp_rate();
        let even_fp_rate = self.even.fp_rate();
        if odd_fp_rate > even_fp_rate {
            odd_fp_rate
        } else {
            even_fp_rate
        }
    }

    /// Returns the current false positive rates.
    ///
    /// In theory the false positive rate `fp_rate` is known at
    /// filter creation. But that, is the theoretical `fp_rate`
    /// that the filter reaches when it is "wasted" because it
    /// has too many entries. Until then, it performs better
    /// than that, statistically.
    ///
    /// As this filter uses two filters under the hood, there
    /// are technically 2 values for the actual false positive rate.
    ///
    /// This function returns both values, starting with the
    /// highest one, which is usually the most significant
    /// as it's reflecting the current behavior of the filter.
    ///
    /// ```
    /// use ofilter::Stream;
    ///
    /// let mut filter: Stream<usize> = Stream::new(1_000);
    /// assert_eq!((0.0, 0.0), filter.fp_rates());
    /// filter.set(&10);
    /// let fp_rates = filter.fp_rates();
    /// assert!(fp_rates.0 > 0.0); // will be greater than params.fp_rate when filter is full
    /// assert!(fp_rates.1 > 0.0); // will be params.fp_rate when filter is full
    /// ```
    pub fn fp_rates(&self) -> (f64, f64) {
        let odd_fp_rate = self.odd.fp_rate();
        let even_fp_rate = self.even.fp_rate();
        if odd_fp_rate > even_fp_rate {
            (odd_fp_rate, even_fp_rate)
        } else {
            (even_fp_rate, odd_fp_rate)
        }
    }

    /// Returns the current ratios between real min fp_rate, and theoretical fp_rate..
    ///
    /// This is a helper to quickly compare the real min `fp_rate`  with
    /// the theoretical `fp_rate`. It compares the min value and not the max,
    /// as it is when the min value reaches 1.0 that the filters are swapped
    /// after one is cleared.
    ///
    /// When this value greater than 1.0, the filter should
    /// soon "age" and clear the most filled underlying Bloom filter.
    ///
    /// ```
    /// use ofilter::Stream;
    ///
    /// let mut filter: Stream<usize> = Stream::new(1_000);
    /// assert_eq!((0.0, 0.0), filter.levels());
    /// filter.set(&10);
    /// let levels = filter.levels();
    /// assert!(levels.0 > 0.0); // will be greater than 1.0 when filter is full
    /// assert!(levels.1 > 0.0); // will be 1.0 when filter is full
    /// ```
    pub fn min_level(&self) -> f64 {
        let odd_level = self.odd.level();
        let even_level = self.even.level();
        if odd_level < even_level {
            odd_level
        } else {
            even_level
        }
    }

    /// Returns the current ratios between real fp_rates, and theoretical fp_rates.
    ///
    /// This is a helper to quickly compare the real `fp_rates`  with
    /// the theoretical `fp_rates`.
    ///
    /// When both of these values are greater than 1.0, the filter should
    /// soon "age" and clear the most filled underlying Bloom filter.
    ///
    /// ```
    /// use ofilter::Stream;
    ///
    /// let mut filter: Stream<usize> = Stream::new(1_000);
    /// assert_eq!((0.0, 0.0), filter.levels());
    /// filter.set(&10);
    /// let levels = filter.levels();
    /// assert!(levels.0 > 0.0); // will be greater than 1.0 when filter is full
    /// assert!(levels.1 > 0.0); // will be 1.0 when filter is full
    /// ```
    pub fn levels(&self) -> (f64, f64) {
        let odd_level = self.odd.level();
        let even_level = self.even.level();
        if odd_level < even_level {
            (odd_level, even_level)
        } else {
            (even_level, odd_level)
        }
    }

    /// Returns the age of the filter.
    ///
    /// The age is the number of times the underlying Bloom filters
    /// have been cleared and swapped.
    ///
    /// In the beginning this is 0, then when filters are full,
    /// one of them is cleared, and age becomes one. Everytime this
    /// happens again, age increases.
    ///
    /// As a consequence, the age gives a raw estimation of how
    /// many different items have been through the cache. With a
    /// given `capacity`, the number of different items the cache
    /// has seen is roughly `age * capacity`. This is just an
    /// approximation.
    ///
    /// ```
    /// use ofilter::{Stream, Params};
    ///
    /// let params = Params{
    ///     nb_hash: 2,
    ///     bit_len: 0,
    ///     nb_items: 1_000,
    ///     fp_rate: 0.1,
    ///     predict: true,
    /// };
    /// let mut filter: Stream<usize> = Stream::new_with_params(params);
    /// assert_eq!(0, filter.age());
    /// for i in 0..100_000 {
    ///     filter.set(&i);
    /// }
    /// println!("{}", filter.age());
    /// assert!(filter.age() >= 90);
    /// assert!(filter.age() <= 100);
    /// ```
    pub fn age(&self) -> usize {
        self.nb_resets
    }

    /// Resize the filter.
    ///
    /// As the underlying Bloom filters are recycled in a
    /// streaming filter, it is possible to resize it.
    ///
    /// What it means is that next time the filter ages,
    /// the new created Bloom filter will have the new
    /// size. It will keep the same number of hash `nb_hash` and
    /// false positive rate `fp_rate`. Only the number
    /// of bits `bit_len` and number of items `nb_items` change.
    ///
    /// ```
    /// use ofilter::Stream;
    ///
    /// let mut filter: Stream<usize> = Stream::new(1_000);
    /// filter.set(&1);
    /// filter.resize(100);
    /// assert_eq!(100, filter.capacity());
    /// assert!(filter.check(&1));
    /// ```
    pub fn resize(&mut self, capacity: usize) {
        let capacity = std::cmp::max(1, capacity);
        let mut params = self.orig_params.clone();
        params.nb_items = capacity;
        params.bit_len = 0;
        self.params = params.adjust();
        self.update_check_period();
    }
}

impl<T> Stream<T>
where
    T: Hash,
{
    /// Record an item in the set.
    ///
    /// Once this has been called, any call to `check()` will return
    /// true, as there are no false negatives. However some other items
    /// may test positive as a consequence of recording this one.
    ///
    /// However, after some time, the item will ultimately disappear
    /// from the set and be replaced by new entries. Only the most
    /// recent entries are guaranteed to be here.
    ///
    /// ```
    /// use ofilter::Stream;
    ///
    /// let mut filter: Stream<usize> = Stream::new(1_000);
    /// filter.set(&42);
    /// ```
    pub fn set(&mut self, item: &T) {
        self.odd.set(item);
        self.even.set(item);
        self.ack_set();
    }

    /// Guess whether an item is likely to be in the set.
    ///
    /// If `set()` has been called before with value, then this returns
    /// true, as there are no false negatives. However it may respond
    /// true even if the item has never been recorded in the set.
    ///
    /// However, if `set()` has been called too long ago and too many
    /// items have been added since, it will test negative anyway.
    ///
    /// ```
    /// use ofilter::Stream;
    ///
    /// let mut filter: Stream<usize> = Stream::new(1_000);
    /// filter.set(&42);
    /// assert!(filter.check(&42));
    /// ```
    pub fn check(&self, item: &T) -> bool {
        self.odd.check(item) || self.even.check(item)
    }

    /// Record an item in the set and returns its previous value.
    ///
    /// Equivalent to calling `get()` then `set()` but performs
    /// hash lookup only once so it's a bit more efficient.
    ///
    /// ```
    /// use ofilter::Stream;
    ///
    /// let mut filter: Stream<usize> = Stream::new(1_000);
    /// assert!(!filter.check_and_set(&42));
    /// assert!(filter.check(&42));
    /// ```
    pub fn check_and_set(&mut self, item: &T) -> bool {
        let found = self.odd.check_and_set(item) || self.even.check_and_set(item);
        self.ack_set();
        found
    }

    fn ack_set(&mut self) {
        self.nb_sets += 1;

        // Calculating levels can be time consuming as it counts
        // ones on both bloom filters and that will certainly
        // requiring reading *ALL* the items in the filter.
        // So just do it every check_period.
        if self.nb_sets % self.check_period != 0 {
            return;
        }

        let odd_level = self.odd.level();
        if odd_level <= 1.0 {
            return;
        }
        let even_level = self.even.level();
        if even_level <= 1.0 {
            return;
        }

        self.nb_resets += 1;

        if odd_level > even_level {
            self.odd = Bloom::new_with_params(self.params.clone());
        } else {
            self.even = Bloom::new_with_params(self.params.clone());
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::collections::HashSet;

    fn new_predict<T>(capacity: usize) -> Stream<T> {
        let mut params = Params::with_nb_items(capacity);
        params.predict = true;
        Stream::new_with_params(params)
    }

    #[test]
    fn test_filter_capacity_0() {
        let filter: Stream<usize> = Stream::new(0);
        assert!(!filter.check(&42));
    }

    #[test]
    fn test_filter_level_avg_dev() {
        let capacities: Vec<usize> = vec![
            10, 100, 200, 500, 800, 1_000, 2_000, 3_000, 4_000, 5_000, 6_000, 7_000, 8_000, 9_000,
            10_000, 20_000, 50_000, 80_000, 100_000, 200_000, 500_000,
        ];
        let mut n: usize = 0;
        let mut levels: Vec<f64> = Vec::new();
        let mut total: f64 = 0.0;
        for capacity in capacities.into_iter() {
            let mut filter: Stream<usize> = new_predict(capacity);
            for i in 0..capacity / 2 {
                filter.set(&i);
            }
            let level = filter.min_level();
            if level > 0.0 {
                total += level;
                levels.push(level);
                n += 1;
            }
            println!("capacity: {} -> level: {}", capacity, level);
            assert!(level < 1.0, "level must be < 1.0");
        }
        let avg_level = total / n as f64;
        let mut sq: f64 = 0f64;
        for level in levels.into_iter() {
            let diff = level - avg_level;
            sq += diff * diff;
        }
        let dev_level = sq.sqrt() / n as f64;
        // We've filled the filter with 50% of its capacity. Since it has 2 hash,
        // 50% of "what it needs to statistically reach the false positive limit"
        // has been filled, so for each hash, there's a 50% probability to wrongly
        // hit a bit that is set for a bad reason, so all in all that level
        // should be 25%.
        println!("level average: {}", avg_level);
        println!("level deviation: {}", dev_level);
        assert!(avg_level < 0.4, "level average must be < 0.3");
        assert!(avg_level > 0.1, "level average must be > 0.2");
        assert!(dev_level < 0.1, "level deviation must be < 0.1");
    }

    #[test]
    fn test_filter_len_ratio() {
        let capacities: Vec<usize> = vec![
            1, 2, 5, 8, 10, 50, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 500, 1_000,
        ];
        let n = capacities.len();
        let mut len_ratios: Vec<f64> = Vec::new();
        let mut total: f64 = 0.0;
        const N: usize = 100;
        for capacity in capacities.into_iter() {
            let mut filter: Stream<usize> = new_predict(capacity);
            for i in 0..capacity * N {
                filter.set(&i);
            }
            let mut set: HashSet<usize> = HashSet::new();
            for i in 0..capacity * N {
                if filter.check(&i) {
                    set.insert(i);
                }
            }
            let len = set.len();
            let len_ratio = len as f64 / capacity as f64;
            total += len_ratio;
            len_ratios.push(len_ratio);
            println!(
                "capacity: {} -> len: {}, ratio: {:0.3}",
                capacity, len, len_ratio
            );
            assert_ne!(0, filter.nb_resets, "there should have been some resets");
            if capacity >= 50 {
                assert!(len_ratio > 1.0, "len should be at least 100% capacity");
                assert!(len_ratio < 25.0, "len should be less than capacity*25");
            }
        }
        let avg_len_ratio = total as f64 / n as f64;
        let mut sq: f64 = 0.0;
        for len_ratio in len_ratios.into_iter() {
            let diff = len_ratio - avg_len_ratio;
            sq += diff * diff;
        }
        let dev_len_ratio = sq.sqrt() / n as f64;
        println!("len ratio average: {}", avg_len_ratio);
        println!("len ratio deviation: {}", dev_len_ratio);
        assert!(avg_len_ratio > 2.0, "len ratio average must be > 2.0");
        assert!(avg_len_ratio < 10.0, "len ratio average must be < 10.0");
        assert!(dev_len_ratio < 1.0, "len ratio deviation must be < 1.0");
    }

    #[test]
    fn test_keeps_relevant_items() {
        let capacities: Vec<usize> = vec![10, 20, 30, 40, 50, 60, 70, 80, 90, 100];
        const N: usize = 1_000;
        for capacity in capacities.into_iter() {
            let mut filter: Stream<usize> = new_predict(capacity);
            for i in 0..capacity * N {
                filter.set(&i);
            }
            let mut set: HashSet<usize> = HashSet::new();
            for i in 0..capacity * N {
                if filter.check(&i) {
                    set.insert(i);
                }
            }
            let len = set.len();
            println!(
                "len: {}, capacity: {}, age: {}, filter: {}",
                len,
                capacity,
                filter.age(),
                filter
            );
            assert!(len > capacity, "len should be greater than capacity");
            for i in (capacity * (N - 1))..(capacity * N) {
                assert!(set.contains(&i), "recent items should be in filter");
            }
        }
    }

    #[test]
    fn test_stream_filtering() {
        let capacities: Vec<usize> = vec![20, 50, 100, 200, 500];
        const N: usize = 1_000;
        for capacity in capacities.into_iter() {
            let mut filter: Stream<usize> = new_predict(capacity);
            let mut set: HashSet<usize> = HashSet::new();
            for i in 0..N {
                for j in 0..capacity {
                    let item = i * capacity + j;
                    filter.set(&item);
                    set.insert(item);
                }
                set.retain(|x| filter.check(x));
            }

            let len = set.len();
            println!(
                "len: {}, capacity: {}, age: {}",
                len,
                capacity,
                filter.age()
            );
            assert!(len > capacity, "len should be greater than capacity");
            assert!(len <= 3 * capacity, "len should be lower than 3*capacity");
            for i in (capacity * (N - 1))..(capacity * N) {
                assert!(set.contains(&i), "recent items should be in filter");
            }
        }
    }
}
