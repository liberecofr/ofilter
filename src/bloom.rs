// Copyright (C) 2024 Christian Mauduit <ufoot@ufoot.org>

use crate::offset_generator::{OffsetGenerator, SEED_SIZE};
use crate::params::Params;
use crate::BUG_REPORT_URL;
use bitvec::prelude::BitVec;
#[cfg(feature = "rand")]
use getrandom;
use siphasher::sip::SipHasher13;
use std::fmt;
use std::hash::{Hash, Hasher};
use std::marker::PhantomData;

/// Bloom filter.
///
/// This is a basic Bloom filter, using [SipHasher13](https://crates.io/crates/siphasher) hash funcs.
/// It can be fine-tuned by passing custom parameters when
/// being initialized. The default constructor uses a
/// false positive rate of 1% and 2 hash functions.
///
/// Heavily inspired from [bloomfilter crate](https://crates.io/crates/bloomfilter).
///
/// # Examples
///
/// ```
/// use ofilter::Bloom;
/// use std::hash::Hash;
///
/// #[derive(Hash)]
/// struct Obj {
///     some: usize,
///     thing: usize,
/// }
///
/// let mut filter: Bloom<Obj> = Bloom::new(100);
/// let obj = Obj{ some: 1, thing: 2};
///
/// assert!(!filter.check(&obj)); // object is not in filter
/// filter.set(&obj);             // put object in filter
/// assert!(filter.check(&obj));  // object is in filter
/// ```
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
#[derive(Debug, Clone)]
pub struct Bloom<T> {
    bit_vec: BitVec,
    params: Params,
    //    sips: Vec<SipHasher13>,
    offset_generator: OffsetGenerator<T>,

    _phantom_data: PhantomData<T>,
}

struct NoRandFeature {}

impl fmt::Display for NoRandFeature {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "no rand support, can not build non-predictable filters, please use `features = [\"rand\"]` in your Cargo.toml file")
    }
}

/// Pretty-print the filter.
///
/// # Examples
///
/// ```
/// use ofilter::Bloom;
///
/// let filter: Bloom<usize> = Bloom::new(100);
///
/// assert_eq!("{ fp_rate: 0.000000, params: { nb_hash: 2, bit_len: 1899, nb_items: 100, fp_rate: 0.009992, predict: false } }" , format!("{}", filter));
/// ```
impl<T> fmt::Display for Bloom<T> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "{{ fp_rate: {:0.6}, params: {} }}",
            self.fp_rate(),
            self.params
        )
    }
}

impl<T> Bloom<T> {
    /// Create a new Bloom filter, with given capacity.
    ///
    /// All other parameters are set to defaults, or aligned to match capacity.
    ///
    /// This type of filter is useful to maintain a set of max `capacity` items.
    /// It can have false positives but no false negatives. This means you
    /// can be sure that an item does *NOT* belong to the set, but only have
    /// a clue whether the item is in the set or not.
    ///
    /// # Examples
    ///
    /// ```
    /// use ofilter::Bloom;
    ///
    /// let filter: Bloom<usize> = Bloom::new(100);
    /// assert_eq!(100, filter.capacity());
    /// ```
    pub fn new(capacity: usize) -> Self {
        let capacity = std::cmp::max(1, capacity);
        Self::new_with_params(Params::with_nb_items(capacity))
    }

    #[cfg(feature = "rand")]
    fn getrandom(dest: &mut [u8]) -> Result<(), getrandom::Error> {
        getrandom::getrandom(dest)
    }

    #[cfg(not(feature = "rand"))]
    fn getrandom(dest: &mut [u8]) -> Result<(), NoRandFeature> {
        Self::predictable_seed(dest)
    }

    fn predictable_seed(seed: &mut [u8]) -> Result<(), NoRandFeature> {
        let mut tmp_seed = [0u8; SEED_SIZE];
        for i in 0..SEED_SIZE {
            tmp_seed[i] = (i * 7) as u8;
        }
        let tmp_sip = SipHasher13::new_with_key(&tmp_seed);

        for i in 0..seed.len() {
            let tmp_sip = &mut tmp_sip.clone();
            i.hash(tmp_sip);
            let hash = tmp_sip.finish();
            seed[i] = hash as u8;
        }
        Ok(())
    }

    /// Create a new Bloom filter, with specific parameters.
    ///
    /// # Examples
    ///
    /// ```
    /// use ofilter::{Bloom, Params};
    ///
    /// let filter: Bloom<usize> = Bloom::new_with_params(Params::with_nb_items_and_fp_rate(100, 0.1));
    /// assert_eq!(100, filter.capacity());
    /// ```
    pub fn new_with_params(params: Params) -> Self {
        let params = params.adjust();

        let mut seed: Vec<u8> = Vec::with_capacity(params.nb_hash * SEED_SIZE);
        for _ in 0..seed.capacity() {
            seed.push(0);
        }
        if params.predict {
            Self::predictable_seed(&mut seed).unwrap_or_else(|e| {
                panic!(
                    "error getting predictable data: {}, please report bugs to <{}>",
                    e, BUG_REPORT_URL
                )
            })
        } else {
            Self::getrandom(&mut seed[..]).unwrap_or_else(|e| {
                panic!(
                    "error getting random data: {}, please report bugs to <{}>",
                    e, BUG_REPORT_URL
                )
            });
        }

        Self::new_with_params_and_seed(params, &seed[..])
    }

    fn new_with_params_and_seed(params: Params, seed: &[u8]) -> Self {
        if seed.len() != params.nb_hash * SEED_SIZE {
            panic!(
                "inconsistent seed size, expected {}, got {}, report bugs to <{}>",
                params.nb_hash * SEED_SIZE,
                seed.len(),
                BUG_REPORT_URL
            );
        }

        let mut bit_vec = BitVec::new();
        bit_vec.resize(params.bit_len, false);

        let offset_generator = OffsetGenerator::new(params.nb_hash, params.bit_len, seed);

        Bloom {
            bit_vec,
            params,
            //sips,
            offset_generator,
            _phantom_data: PhantomData,
        }
    }

    /// Get filter params.
    ///
    /// This can be useful because when creating the filter, the `.adjust()`
    /// func is called, and may decide to fine-tune some parameters. With this,
    /// one can know exactly what is used by the filter.
    ///
    /// # Examples
    ///
    /// ```
    /// use ofilter::Bloom;
    ///
    /// let filter: Bloom<usize> = Bloom::new(100);
    /// println!("{}", filter.params());
    /// ```
    pub fn params(&self) -> Params {
        self.params.clone()
    }

    /// Get filter capacity.
    ///
    /// Returns the value of `params.nb_items`, that is the number of
    /// items the filter is designed for.
    ///
    /// ```
    /// use ofilter::{Bloom, Params};
    ///
    /// let filter: Bloom<usize> = Bloom::new_with_params(Params::with_bit_len(1_000_000));
    /// assert_eq!(52681, filter.capacity());
    /// ```
    pub fn capacity(&self) -> usize {
        self.params.nb_items
    }

    /// Clear the filter.
    ///
    /// Clears the bit vector, but keeps parameters.
    ///
    /// ```
    /// use ofilter::Bloom;
    ///
    /// let mut filter: Bloom<usize> = Bloom::new(1_000);
    /// filter.set(&10);
    /// assert!(filter.check(&10));
    /// filter.clear();
    /// assert!(!filter.check(&10));
    /// ```
    pub fn clear(&mut self) {
        self.bit_vec.fill(false);
    }

    /// Returns true if filter is empty.
    ///
    /// ```
    /// use ofilter::Bloom;
    ///
    /// let mut filter: Bloom<usize> = Bloom::new(1_000);
    /// assert!(filter.is_empty());
    /// filter.set(&10);
    /// assert!(!filter.is_empty());
    /// ```
    pub fn is_empty(&self) -> bool {
        self.bit_vec.not_any()
    }

    /// Returns the current false positive rate.
    ///
    /// In theory the false positive rate `fp_rate` is known at
    /// filter creation. But that, is the theoretical `fp_rate`
    /// that the filter reaches when it is "wasted" because it
    /// has too many entries. Until then, it performs better
    /// than that, statistically.
    ///
    /// This function returns the actual false positive rate.
    /// When this value is greater than the `fp_rate` from
    /// the parameters, one should not continue to add items
    /// to the filter.
    ///
    /// ```
    /// use ofilter::Bloom;
    ///
    /// let mut filter: Bloom<usize> = Bloom::new(1_000);
    /// assert_eq!(0.0, filter.fp_rate());
    /// filter.set(&10);
    /// assert!(filter.fp_rate() > 0.0); // will be params.fp_rate when filter is full
    /// ```
    pub fn fp_rate(&self) -> f64 {
        let ones_rate = self.bit_vec.count_ones() as f64 / self.bit_vec.len() as f64;
        ones_rate.powi(self.params.nb_hash as i32)
    }

    /// Returns the ratio between real fp_rate, and theoretical fp_rate.
    ///
    /// This is a helper to quickly compare the real `fp_rate`, given
    /// how filled the filter is, and the theoretical `fp_rate`.
    ///
    /// When this value is greater than 1.0, one should not
    /// continue to add items to the filter.
    ///
    /// ```
    /// use ofilter::Bloom;
    ///
    /// let mut filter: Bloom<usize> = Bloom::new(1_000);
    /// assert_eq!(0.0, filter.level());
    /// filter.set(&10);
    /// assert!(filter.level() > 0.0); // will be 1.0 when filter is full
    /// ```
    pub fn level(&self) -> f64 {
        self.fp_rate() / self.params.fp_rate
    }

    /// Returns true if there is still room in the filter.
    ///
    /// This is a helper which returns true if the actual
    /// `fp_rate` is lower than the theoretical `fp_rate`.
    ///
    /// When this is false, one should not
    /// continue to add items to the filter.
    ///
    /// ```
    /// use ofilter::Bloom;
    ///
    /// let mut filter: Bloom<usize> = Bloom::new(1_000);
    /// assert_eq!(0.0, filter.level());
    /// filter.set(&10);
    /// assert!(filter.level() > 0.0); // will be 1.0 when filter is full
    /// ```
    pub fn is_ok(&self) -> bool {
        self.fp_rate() <= self.params.fp_rate
    }

    /// Returns the number of ones in the bit vector.
    ///
    /// By comparing this number with the size of the bit vector,
    /// one can estimate how "filled" the filter is.
    ///
    /// ```
    /// use ofilter::Bloom;
    ///
    /// let mut filter: Bloom<usize> = Bloom::new(1_000_000);
    /// assert_eq!(0, filter.count_ones());
    /// filter.set(&10);
    /// assert_eq!(2, filter.count_ones());
    /// ```
    pub fn count_ones(&self) -> usize {
        self.bit_vec.count_ones()
    }

    /// Export the underlying bit vector.
    ///
    /// ```
    /// use ofilter::Bloom;
    ///
    /// let mut filter: Bloom<usize> = Bloom::new(1_000_000);
    /// let bit_vec = filter.export();
    /// println!("{:?}", bit_vec);
    /// ```
    pub fn export(&self) -> &BitVec {
        &self.bit_vec
    }

    /// Import the underlying bit vector.
    ///
    /// ```
    /// use ofilter::Bloom;
    /// use bitvec::prelude::BitVec;
    ///
    /// let mut filter: Bloom<usize> = Bloom::new(1_000_000);
    /// let mut bit_vec = BitVec::new();
    /// bit_vec.resize(100, false);
    /// bit_vec.set(42, true);
    /// filter.import(&bit_vec); // safe to call even is sizes mismatch
    /// ```
    pub fn import(&mut self, src: &BitVec) {
        self.bit_vec = src.clone();
        self.bit_vec.resize(self.params.nb_items, false);
    }
}

impl<T> Bloom<T>
where
    T: Hash,
{
    /// Record an item in the set.
    ///
    /// Once this has been called, any call to `check()` will return
    /// true, as there are no false negatives. However some other items
    /// may test positive as a consequence of recording this one.
    ///
    /// ```
    /// use ofilter::Bloom;
    ///
    /// let mut filter: Bloom<usize> = Bloom::new(1_000);
    /// filter.set(&42);
    /// ```
    pub fn set(&mut self, item: &T) {
        for offset in self.offset_generator.iter(item) {
            self.bit_vec.set(offset, true);
        }
    }

    /// Guess whether an item is likely to be in the set.
    ///
    /// If `set()` has been called before with value, then this returns
    /// true, as there are no false negatives. However it may respond
    /// true even if the item has never been recorded in the set.
    ///
    /// ```
    /// use ofilter::Bloom;
    ///
    /// let mut filter: Bloom<usize> = Bloom::new(1_000);
    /// filter.set(&42);
    /// assert!(filter.check(&42));
    /// ```
    pub fn check(&self, item: &T) -> bool {
        for offset in self.offset_generator.iter(item) {
            if !self.bit_vec.get(offset).unwrap() {
                return false;
            }
        }
        true
    }

    /// Record an item in the set and returns its previous value.
    ///
    /// Equivalent to calling `get()` then `set()` but performs
    /// hash lookup only once so it's a bit more efficient.
    ///
    /// ```
    /// use ofilter::Bloom;
    ///
    /// let mut filter: Bloom<usize> = Bloom::new(1_000);
    /// assert!(!filter.check_and_set(&42));
    /// assert!(filter.check(&42));
    /// ```
    pub fn check_and_set(&mut self, item: &T) -> bool {
        let mut found = true;
        for offset in self.offset_generator.iter(item) {
            if !self.bit_vec.get(offset).unwrap() {
                found = false;
                self.bit_vec.set(offset, true);
            }
        }
        found
    }
}
