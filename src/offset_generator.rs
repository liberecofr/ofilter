// Copyright (C) 2024 Christian Mauduit <ufoot@ufoot.org>

use crate::BUG_REPORT_URL;
use siphasher::sip::SipHasher13;
use std::hash::{Hash, Hasher};
use std::marker::PhantomData;

pub(crate) const SEED_SIZE: usize = 16;
// Reserve a few bits which are not going to be consumed to
// generate the offsets, but are useful so that the final
// modulo still yieds some "flat" result and does not stack
// everything in the lower values. I never actually witnessed
// any problem with this, but better safe than sorry.
const MODULO_EXTRA_BITS: usize = 10;

// This private struct is used to:
//
// 1) encapsulate the logic of producing offsets in bit vectors
// 2) optimize it by using all the bits of a given hash.
//
// The idea is that once you do a siphasher call, you have
// 64 bits of available hash data, all of which are independant
// in the sense that you can use chunks of them independantly,
// there's enough entropy for that.
//
// So when the offsets we need can be encoded in say, 16 bits
// then we can get 3*16=48 bits of data from a 64-bit buffer,
// and therefore use only once call to siphasher and pretend
// we have 3 different hash.
//
// We keep a few high weight bits (10 of them) to avoid bias
// on the last hash, so we work on 54 bits only, but that's
// already something.
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
#[derive(Debug, Clone)]
pub(crate) struct OffsetGenerator<T> {
    nb_hash: usize,
    bit_len: usize,
    nb_bits_per_offset: usize,
    nb_offset_per_hash: usize,
    sips: Vec<SipHasher13>,
    _phantom_data: PhantomData<T>,
}

impl<T> OffsetGenerator<T> {
    pub(crate) fn new(nb_hash: usize, bit_len: usize, seed: &[u8]) -> Self {
        let nb_bits_per_offset = std::cmp::max(1, usize::BITS - bit_len.leading_zeros()) as usize;
        let nb_offset_per_hash = std::cmp::max(
            1,
            (u64::BITS as usize - MODULO_EXTRA_BITS) / nb_bits_per_offset,
        );
        let mut l: usize = 0;
        let mut sips: Vec<SipHasher13> = Vec::new();
        while l < nb_hash {
            let i = sips.len();
            if seed.len() < (i + 1) * SEED_SIZE {
                panic!(
                    "seed is not long enough, expected {}, got {}, report bugs to <{}>",
                    nb_hash * SEED_SIZE,
                    seed.len(),
                    BUG_REPORT_URL
                );
            }
            let mut tmp_seed = [0u8; SEED_SIZE];
            tmp_seed.copy_from_slice(&seed[i * SEED_SIZE..(i + 1) * SEED_SIZE]);
            sips.push(SipHasher13::new_with_key(&tmp_seed));
            l += nb_offset_per_hash;
        }

        OffsetGenerator {
            nb_hash,
            bit_len,
            nb_bits_per_offset,
            nb_offset_per_hash,
            sips,
            _phantom_data: PhantomData,
        }
    }
}

impl<T> OffsetGenerator<T>
where
    T: Hash,
{
    pub(crate) fn iter<'a>(&'a self, item: &'a T) -> OffsetGeneratorIter<'a, T> {
        OffsetGeneratorIter {
            cur_hash: 0,
            cur_sip: 0,
            cur_offset: 0,
            generator: self,
            item,
            last_generated: self.generate_from_sip(0, item),
        }
    }

    #[inline]
    fn generate_from_sip(&self, hash_offset: usize, item: &T) -> u64 {
        let sip = &mut self.sips[hash_offset].clone();
        item.hash(sip);
        sip.finish()
    }
}

pub(crate) struct OffsetGeneratorIter<'a, T>
where
    T: Hash,
{
    cur_hash: usize,
    cur_sip: usize,
    cur_offset: usize,
    generator: &'a OffsetGenerator<T>,
    item: &'a T,
    last_generated: u64,
}

impl<T> Iterator for OffsetGeneratorIter<'_, T>
where
    T: Hash,
{
    type Item = usize;

    fn next(&mut self) -> Option<usize> {
        if self.cur_hash >= self.generator.nb_hash {
            return None;
        }
        let offset = self.last_generated as usize % self.generator.bit_len;
        self.last_generated >>= self.generator.nb_bits_per_offset;
        self.cur_offset += 1;
        self.cur_hash += 1;
        if self.cur_offset >= self.generator.nb_offset_per_hash {
            self.cur_offset = 0;
            self.cur_sip += 1;
            if self.cur_sip < self.generator.sips.len() {
                self.last_generated = self.generator.generate_from_sip(self.cur_sip, self.item);
            }
        }
        Some(offset)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_one_per_hash() {
        const S: usize = 80;
        let seed = [7; S];
        let gen: OffsetGenerator<usize> = OffsetGenerator::new(5, 1_000_000_000, &seed);
        assert_eq!(5, gen.nb_hash);
        assert_eq!(1_000_000_000, gen.bit_len);
        assert_eq!(30, gen.nb_bits_per_offset);
        assert_eq!(1, gen.nb_offset_per_hash);
        assert_eq!(5, gen.sips.len());
        assert_eq!(5, gen.iter(&0).count());
    }

    #[test]
    fn test_3_per_hash() {
        const S: usize = 16;
        let seed = [7; S];
        let gen: OffsetGenerator<usize> = OffsetGenerator::new(4, 20, &seed);
        assert_eq!(4, gen.nb_hash);
        assert_eq!(20, gen.bit_len);
        assert_eq!(5, gen.nb_bits_per_offset);
        assert_eq!(10, gen.nb_offset_per_hash);
        assert_eq!(1, gen.sips.len());
        assert_eq!(4, gen.iter(&0).count());
    }
}
