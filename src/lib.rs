// Copyright (C) 2024 Christian Mauduit <ufoot@ufoot.org>

//! [OFilter](https://gitlab.com/liberecofr/ofilter) is a fast thread-safe Bloom filter.
//!
//! ![OFilter icon](https://gitlab.com/liberecofr/ofilter/raw/main/ofilter.png)
//!
//! # Examples
//!
//! Basic Bloom filter:
//!
//! ```
//! use ofilter::Bloom;
//!
//! let mut filter: Bloom<usize> = Bloom::new(100);
//! filter.set(&42);
//! assert!(filter.check(&42));
//! ```
//!
//! Thread-safe version:
//!
//! ```
//! use ofilter::SyncBloom;
//!
//! // does not need to be `mut`
//! let filter: SyncBloom<usize> = SyncBloom::new(100);
//! filter.set(&42);
//! assert!(filter.check(&42));
//! ```
//!
//! Streaming Bloom filter:
//!
//! ```
//! use ofilter::Stream;
//! use std::collections::HashSet;
//!
//! let mut set: HashSet<usize> = HashSet::new();
//! let mut filter: Stream<usize> = Stream::new(100);
//! for i in 0..1_000 {
//!     for j in 0..1_000 {
//!         let item = i * 1_000 + j;
//!         filter.set(&item);
//!         set.insert(item);
//!     }
//!     set.retain(|x| filter.check(x));
//! }
//! println!("{}", set.len());
//! // Out of 1 million entries, only a few hundreds left
//! assert!(set.len() < 500);
//! ```
//!
//! Thread-safe streaming version:
//!
//! ```
//! use ofilter::SyncStream;
//! use std::collections::HashSet;
//!
//! let mut set: HashSet<usize> = HashSet::new();
//! // does not need to be `mut`
//! let filter: SyncStream<usize> = SyncStream::new(100);
//! for i in 0..1_000 {
//!     for j in 0..1_000 {
//!         let item = i * 1_000 + j;
//!         filter.set(&item);
//!         set.insert(item);
//!     }
//!     set.retain(|x| filter.check(x));
//! }
//! println!("{}", set.len());
//! // Out of 1 million entries, only a few hundreds left
//! assert!(set.len() < 500);
//! ```

/// URL to report bugs.
///
/// This is used internally to provide context when something unexpected happens,
/// so that users can find find out which piece of software fails,
/// and how to contact author(s).
///
/// Alternatively, send a direct email to <ufoot@ufoot.org>.
pub const BUG_REPORT_URL: &str = "https://gitlab.com/liberecofr/ofilter/-/issues";

mod bloom;
mod offset_generator;
mod params;
mod stream;
mod sync_bloom;
mod sync_stream;

pub use bitvec::prelude::BitVec;
pub use bloom::*;
pub use params::*;
pub use stream::*;
pub use sync_bloom::*;
pub use sync_stream::*;
